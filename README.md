# Docker Quickstart

This guide provides step-by-step instructions for building and running Docker containers using a simple example.

## Prerequisites

- [Docker](https://docs.docker.com/get-docker/) must be installed on your system.

## Getting Started

### 1. Build a Docker Image

Use the following command to build a Docker image named "amirimg" from the current directory:

```bash
docker build -t amirimg .
```
- -t amirimg tags the image with the name "amirimg."

### 2. Run a Docker Container
Step 1: Run a Container (without specifying an image)
Run a Docker container in detached mode with the name "amirhttp":

```bash
docker run -it -d --name amirhttp
```
- -it allocates a pseudo-TTY and keeps STDIN open for interactive use.
- -d runs the container in detached mode (in the background).

Step 2: Run a Container (using the "amirimg" image)
To run a Docker container using the "amirimg" image you built earlier:
```bash
docker run -it -d --name amirhttp amirimg
```
The command specifies the image to use.

### 3. View Running Containers
To view information about all running Docker containers, use the following command:
```bash
docker ps
```
Additional Information
- To view information about all containers (including stopped ones), use docker ps -a.
- Remember that container names must be unique. If you want to run multiple containers simultaneously, give them different names.

## To upload a Docker image to Docker Hub, you'll need to follow these steps:
### Tag the Image:
```bash
docker tag amirimg your-dockerhub-username/amirimg
```
### Login to Docker Hub:
```bash
docker login
```
### Push the Image:
```bash
docker push your-dockerhub-username/amirimg
```


Cleanup
To stop and remove a container, use docker stop CONTAINER_ID and docker rm CONTAINER_ID. To remove an image, use docker rmi IMAGE_ID.
