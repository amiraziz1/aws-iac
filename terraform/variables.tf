# Defining Region
variable "aws_region" {
  default = "us-east-1"
}


variable "amiraziz_vpc_cidr" {
  description = "The CIDR block for the VPC in the main configuration."
  type        = string
  default     = "10.0.0.0/16"
}

