# code for intuitive
module "amir_vpc" {
  source = "./module/vpc_module" 

  vpc_cidr = var.amiraziz_vpc_cidr
}

module "subnets" {
  source = "./module/subnet_module"  
  vpc_id = module.amir_vpc.vpc_id 
}

module "amir_ec2_instances" {
  source = "./module/ec2_module"  
  subnet_ids = module.subnets.subnet_ids
}

module "amir_s3_bucket" {
  source = "./module/s3_module"  
  bucket_name = "amir-aziz-bucket-aws-lab" 
}

output "vpc_id" {
  value = module.amir_vpc.vpc_id
}
