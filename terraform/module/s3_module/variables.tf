variable "bucket_name" {
  description = "The name of the S3 bucket."
  type        = string
  default = "default-example-bucket-aws-lab"
}