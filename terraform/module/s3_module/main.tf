resource "aws_s3_bucket" "amir_bucket" {
  bucket = var.bucket_name
  
  tags = {
    Name = var.bucket_name
  }
}