variable "subnet_ids" {
  description = "List of subnet IDs for the EC2 instances."
  type        = list(string)
}

resource "aws_instance" "ec2_instances" {
  count = 2

  ami           = "ami-0df435f331839b2d6"  
  instance_type = "t2.micro"              
  subnet_id     = element(var.subnet_ids, count.index)  
  tags = {
    Name = "EC2-Instance-${count.index}"
  }

  root_block_device {
    volume_size = 20  
    volume_type = "gp2"  
  }
}