variable "vpc_id" {
  description = "The ID of the VPC where subnets will be created."
  type        = string
}


resource "aws_subnet" "subnets" {
  count          = length(var.subnet_cidrs)
  vpc_id         = var.vpc_id
  cidr_block     = var.subnet_cidrs[count.index]
  availability_zone = "us-east-1a" 
}

output "subnet_ids" {
  value = aws_subnet.subnets[*].id
}

