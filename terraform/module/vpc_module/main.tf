resource "aws_vpc" "amirvpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "AmirAziz-VPC"
  }
}

output "vpc_id" {
  value = aws_vpc.amirvpc.id
}
